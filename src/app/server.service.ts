import {Http, Response} from '@angular/http';
import {Injectable} from '@angular/core';
import 'rxjs-compat/add/operator/map';
import {Observable} from 'rxjs/observable';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';


@Injectable()
export class ServerService {
  constructor(private http: Http) {}
  storeServers(servers: any[]) {
    const headers = new Headers({'Content-Type': 'application/json'});
    // return this.http.post('https://udemy-ng-http-4d26d.firebaseio.com/data.json',
    //   servers
    //   );
return this.http.put('https://udemy-ng-http-4d26d.firebaseio.com/data.json',
      servers
      );
  }
  getServers() {
    return this.http.get('https://udemy-ng-http-4d26d.firebaseio.com/data.json')
      .map(
        (response: Response) => {
          const data = response.json();
          for (const server of data) {
            server.name = 'FETCHED_' + server.name;
          }
          return data;
        }
      )
      .pipe(catchError(error => {
        return throwError(error);
      }));
  }
  getAppName() {
    return this.http.get('https://udemy-ng-http-4d26d.firebaseio.com/appName.json')
      .map(
        (response: Response) => {
          return response.json();        }
      );
  }
}
